// Initialise all the fucking constants we need in this game as well as any
// global variables.

const BOARDWIDTH = 16;
const BOARDHEIGHT = 16;
const BLOCKWIDTH =  16;
const BLOCKHEIGHT = 16;
const BOMBNUMBER = 40;

const GUIHEIGHT = 64;
const CANVASWIDTH = BOARDWIDTH*(BLOCKWIDTH+1);
const CANVASHEIGHT = BOARDHEIGHT*(BLOCKHEIGHT+1) + GUIHEIGHT;


var board = new Array(BOARDWIDTH);

var flags = BOMBNUMBER;
var time = 0;
var firstClick = true;
var flagsText;
var face;
var timeText;
var death = true;

// Schema for the block sprite sheet is:
const DEFAULT = 11;
// States 1 through 8 are their respective numbers.
const FLAGGED = 9;
const BOMB = 10;
const EMPTY = 0;
const FALSEFLAG = 12;

var game = new Phaser.Game(CANVASWIDTH, CANVASHEIGHT, Phaser.AUTO, "gameDiv", {
  preload: preload, create: create, update: update
});

function preload(){
  // Load the spritesheets: Block, gui and GameOver
  game.load.image("gui", "assets/gui.png");
  game.load.spritesheet("block", "assets/block.png", 16, 16, 13, 0, 1);
  game.load.spritesheet("gameOver", "assets/gameover.png", 32, 32, 3, 0, 1);
}

function create(){
  // All the shit is made. A.K.A
  // 1. The board is made, and assigned sprites.
  // 2. The bombs are created using a while loop, that loops back if it detects
  // that the selected block is already a bomb.
  // 3. Check every block on the board's number of directly connected bombs. Do
  // this by interating over them twice from -1 to 1 for both the x and y coords
  // and if a bomb is detected at that location add to the count. After all 8
  // surronding blocks have been checked, set the blocks state to the count.


  game.add.sprite(0, 0, "gui");//.scale.setTo(1.5,1.5);

  // Makes the board by iterating over each part of it
  for (var i = 0; i < BOARDWIDTH; i++){
    board[i] = new Array(BOARDHEIGHT);
    for (var j = 0; j < BOARDHEIGHT; j++){
      // Set a sprite to each block with 1 pixel borders.
      board[i][j] = game.add.sprite(i * (BLOCKWIDTH + 1), j * (BLOCKHEIGHT + 1) + GUIHEIGHT, "block");
      //board[i][j].scale.setTo(1.5,1.5);
      // Set every blocks state to empty and default as its starting frame.
      board[i][j].state = EMPTY;
      board[i][j].frame = DEFAULT;
      // Assign the x and y placement on the baord to make it easier to get the
      // position.
      board[i][j].i = i;
      board[i][j].j = j;
      // Allow mouse input on blocks.
      board[i][j].inputEnabled = true;
    }
  }

  flagsText = game.add.text(44, 16, flags);
  face = game.add.sprite(120, 16, "gameOver");
  face.inputEnabled = true;
  face.frame = 0;
  timeText = game.add.text(196, 16, 0);


}

function update(){
  // Main game loop.
  // Check if first click, if it is then create bombs and state.

  for (var i = 0; i < BOARDWIDTH; i++){
    for (var j = 0; j < BOARDHEIGHT; j++){
      board[i][j].events.onInputDown.add(clicked, this);
    }
  }

  face.events.onInputDown.add(faceReset, this);

  if (death === false){
    time = time + game.time.elapsed;
  }
  flagsText.text = flags;
  timeText.text = Math.floor((time/1000));

  // If every non-bomb clicked

  var allDone = BOARDWIDTH * BOARDHEIGHT - BOMBNUMBER;
  for (var i = 0; i < BOARDWIDTH; i++){
    for (var j = 0; j < BOARDHEIGHT; j++){
      if(board[i][j].frame === board[i][j].state
        && board[i][j].state !== BOMB){
        allDone -= 1;
      }
    }
  }
  if (allDone === 0){
    win();
  }
}

function faceReset(sprite, pointer){
  for (var i = 0; i < BOARDWIDTH; i++){
    for (var j = 0; j < BOARDHEIGHT; j++){
      board[i][j].inputEnabled = true;
      board[i][j].state = EMPTY;
      board[i][j].frame = DEFAULT;
    }
  }
  flags = BOMBNUMBER;
  face.frame = 0;
  firstClick = true;
}

function clicked(sprite, pointer){
  if(firstClick === true){
    setStates(BOMBNUMBER, sprite.i, sprite.j);
    firstClick = false;
    death = false;
  }
  if(pointer.rightButton.isDown){
    if(sprite.frame === DEFAULT){
       sprite.frame = FLAGGED;
       flags -= 1;
     }
    else if (sprite.frame === FLAGGED){
       sprite.frame = DEFAULT;
       flags += 1;
     }
  }
  else if (pointer.leftButton.isDown && sprite.frame === DEFAULT){
    if(sprite.state === EMPTY){
      fill([sprite]);
      sprite.frame = sprite.state;
    }
    else if(sprite.state === BOMB){
      gameOver();
    }
    else if (sprite.frame !== FLAGGED){
      // Any numbers are shown if left clicked.
      sprite.frame = sprite.state;
    }
  }
}

function fill(stack){
  if (stack.length != undefined){

    x = stack[stack.length - 1].i;
    y = stack[stack.length - 1].j;
    stack.pop();



    while ( (y - 1) >= 0 && board[x][y - 1].state === EMPTY){
      y -= 1;
    }
    if ((y - 1) >= 0 && board[x][y - 1].state !== EMPTY){
      board[x][y - 1].frame = board[x][y - 1].state;
    }
    else if ((y - 1) >= 0 && board[x][y - 1] === EMPTY){
      board[x][y - 1].frame = board[x][y - 1].state;
      stack = stack.concat(checkHorz(x, (y-1), -1));
      stack = stack.concat(checkHorz(x, (y-1), 1));
    }

    while (y < BOARDHEIGHT && board[x][y].state === EMPTY){
      board[x][y].frame = board[x][y].state;

      stack = stack.concat(checkHorz(x, (y), -1));
      stack = stack.concat(checkHorz(x, (y), 1));
      y += 1;
    }

    if (y < BOARDHEIGHT){
      board[x][y].frame = board[x][y].state;
      stack = stack.concat(checkHorz(x, (y), -1));
      stack = stack.concat(checkHorz(x, (y), 1));

    }
  }

  if (stack.length !== 0){
    fill(stack);
  }

}

function checkHorz(x, y, dif){
  var stack = [];

  if ((x + dif) < BOARDWIDTH
    && (x + dif) >= 0){
    // Checks if its at the top and a hidden empty one, if it is then it auto
    // adds it to the stack.
    if (y === 0
      && board[x + dif][y].state === EMPTY
      && board[x + dif][y].frame === DEFAULT){
      stack.push(board[x + dif][y]);
    }
    // Checks if the block to the side is not of empty state and changes it.
    else if (board[x + dif][y].state !== EMPTY){
      //console.log("The block is being entered");
      board[x + dif][y].frame = board[x + dif][y].state;
    }

    // Checks if the block to the side is empty. If it is and the block above is
    // not then it adds the block to the stack.
    else if (board[x + dif][y].state === EMPTY
      && board[x + dif][y].frame === DEFAULT){
        stack.push(board[x + dif][y]);
    }
  }

  return stack;

}

function win(){
  for (var i = 0; i < BOARDWIDTH; i++){
    for (var j = 0; j < BOARDHEIGHT; j++){
      board[i][j].inputEnabled = false;
      if (board[i][j].state === BOMB){
          board[i][j].frame = FLAGGED;
      }
    }
  }
  face.frame = 2;

}
function gameOver(){
  // Change Face to frame two
  // Stop Time update by saveing to new function?
  // loop through whole board if bomb, show if flagged and not bomb,
  // false flag.
  for (var i = 0; i < BOARDWIDTH; i++){
    for (var j = 0; j < BOARDHEIGHT; j++){
      board[i][j].inputEnabled = false;
      if (board[i][j].state !== BOMB && board[i][j].frame === FLAGGED){
        board[i][j].frame = FALSEFLAG;
      }
      else if (board[i][j].state === BOMB){
          board[i][j].frame = BOMB;
      }
      death = true;
    }
  }

  face.frame = 1;
}

function setStates(bombNum, xres, yres){
  // Makes bombNum of randomly placed bombs and if the randomly picked place is
  // already of state bomb or has the same coordinates as xres or yres you
  // repeate the bomb making process. Makes a invernable 3*3 block around the
  // area you clicked.

  board[xres][yres].state = EMPTY;

  for (var i = 0; i < bombNum; i++){
    var notBomb = true;
    while(notBomb === true){
      x = Math.floor(Math.random() * (0, BOARDWIDTH));
      y = Math.floor(Math.random() * (0, BOARDHEIGHT));
      if(board[x][y].state !== BOMB
        && !(x === xres + 1 && y === yres + 1)
        && !(x === xres + 1 && y === yres)
        && !(x === xres + 1 && y === yres - 1)
        && !(x === xres && y === yres + 1)
        && !(x === xres && y === yres)
        && !(x === xres && y === yres - 1)
        && !(x === xres - 1 && y === yres + 1)
        && !(x === xres - 1 && y === yres)
        && !(x === xres - 1 && y === yres - 1)
      ){
        board[x][y].state = BOMB;
        notBomb = false;
      }
    }
  }

  // Set the states for each block after the bomb has been initialised.
  for (var i = 0; i < BOARDWIDTH; i++){
    for (var j = 0; j < BOARDHEIGHT; j++){
      if (board[i][j].state !== BOMB){
        bombCount = 0;
        for (var x = -1; x < 2; x++){
          for (var y = -1; y < 2; y++){
            if ((i + x) >= 0
              && (i + x) < BOARDWIDTH
              && (j + y) >= 0
              && (j + y) < BOARDHEIGHT
              && board[i + x][j + y].state === BOMB){

              bombCount += 1;
            }
          }
        }
        board[i][j].state = bombCount;
      }
    }
  }
}
