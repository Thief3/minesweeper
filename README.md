# Minesweeper: The cheap version

## Description

I made this mostly as a method to learn phaser.io and get back into minesweeper.
It's your typical 16*16, 40 bomb minesweeper board.

## Credits

Programming - Malik "Thief3" "Mulk" Kissarli

Art - Joshua Robertson "J Robotson"

## Licenses

The code is under the MIT license, while the art is all owned by Joshua
Robertson. If you are interested in using his art contact him at:
http://j-robotson.tumblr.com/
